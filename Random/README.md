## Random/Other ##
This folder contains the code any code that needs to be sorted or does not have a specific folder. These can also be concept ideas or things not related to the general theme



- If a file has not been worked on you are free to edit it in any way you deem.
- If a file is in the process of being edited but you would like to work on it as well, please create a new branch and we can decide between both at a later date.
-If you have ideas that can be incorporated into this folders general idea, feel free to create another file.    




----------
*Any additional custom recipes should be also noted on the Custom Recipe.txt in the main folder of the repo. *                     