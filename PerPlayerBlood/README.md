## Altered Mobs ##
This folder contains code in which vanilla mobs have been altered whether rates or special designs.



- If a file has not been worked on you are free to edit it in any way you deem.
- If a file is in the process of being edited but you would like to work on it as well, please create a new branch and we can decide between both at a later date.
- If you think of more mobs or ideas that can be incorporated into this folders general idea, feel free to create another file.    




----------
*Any additional custom recipes should be also noted on the Custom Recipe.txt in the main folder of the repo. *                     